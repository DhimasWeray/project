<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FileUploadController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('magang.master');
// });

Route::get('/', 'TestiController@index');

Route::get('/program', 'ProgramController@post');
Route::get('/program/{id}' ,'ProgramController@rincian');

Route::get('/admin', 'ProgramController@create');
Route::post('/admin', 'ProgramController@store');

Route::get('/admin/show', 'ProgramController@index');

Route::get('/admin/edit/{id}', 'ProgramController@edit');
Route::put('/admin/update/{id}', 'ProgramController@update');

Route::delete('/admin/delete/{id}', 'ProgramController@destroy');

Route::get('/form', 'MagangController@create');
Route::post('/formDaftar', 'MagangController@store');
Route::get('/formDaftar', 'MagangController@select');//ga kepakai

Route::get('/formDaftar/show', 'MagangController@index');

Route::get('/formDaftar/edit/{id}', 'MagangController@edit');
Route::put('/formDaftar/update/{id}', 'MagangController@update');

Route::get('/formDaftar/editUser/{id}', 'MagangController@editUser');


Route::delete('/formDaftar/delete/{id}', 'MagangController@destroy');

Route::post('/daftar/program','PendaftarController@store');
Route::get('/admin/program/show','PendaftarController@index');

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/testi', 'TestiController@create');//create
Route::post('/testi', 'TestiController@store');//kirim data
Route::get('/testi/show', 'TestiController@index');//read