<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="TemplateMo">
    <link href="{{asset('https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap')}}" rel="stylesheet">
    <!-- {{asset('assets/css/fontawesome.css')}} -->

    <title>Finance Business HTML5 Template</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/templatemo-finance-business.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.css')}}">
<!--

Finance Business TemplateMo

https://templatemo.com/tm-545-finance-business

-->
  </head>

  <body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->

    <!-- Navbar -->
@include('magang.layout.navbar')


    <!-- Page Content -->
    <!-- Banner Starts Here -->
@include('magang.layout.main')
    <!-- Banner Ends Here -->


@include('magang.layout.benefit')
@include('magang.layout.mitra')

    <div class="request-form">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <h4>More Questions?</h4>
            <span>Lets talk business. With our free consultation service. So, what are you waiting for ?</span>
          </div>
          <div class="col-md-4" >
            <style>
              footer {
                margin: auto;
                content-align:center;
              }
              html{
                scroll-behavior: smooth;
              }
            </style>
            <a href="#footer" id class="border-button">Contact Us</a>
          </div>
        </div>
      </div>
    </div>

@yield('testi')
@include('magang.layout.testi')
    <!-- Footer Starts Here -->
<div id="footer" class="main">
@include('magang.layout.footer')
</div>
    <div class="sub-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <p>Copyright &copy; 2021 PT.IT Solution
          </p>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Additional Scripts -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="{{asset('assets/js/owl.js')}}"></script>
    <script src="{{asset('assets/js/slick.js')}}"></script>
    <script src="{{asset('assets/js/accordions.js')}}"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>

  </body>
</html>