
    <div class="fun-facts">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="left-content">
              <span>About</span>
              <h2>Our solutions for your <em>business growth</em></h2>
              <p>In this digital age, we believe that connectivity is what it takes to build a successful business empire. Having websites or mobile applications that focuses on the user and is easily understandable will boost your competitiveness in the market.

With the mission to get our clients what they truly deserve, Vodjo was founded on November 2015 in Bandung, Indonesia.<br><br> We are a team of talented and passionate developers, designers and technopreneurs who thrive on designing and developing great user interfaces and experiences for websites, web applications and mobile applications. The diversity in our team generates a truly unique experiences for our client.</p>
              
            </div>
          </div>
          <div class="col-md-6 align-self-center">
            <div class="row">
              <div class="col-md-6">
                <div class="count-area-content">
                  <div class="count-digit">24</div>
                  <div class="count-title">Work Hours</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="count-area-content">
                  <div class="count-digit">1280</div>
                  <div class="count-title">Great Reviews</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="count-area-content">
                  <div class="count-digit">578</div>
                  <div class="count-title">Projects Done</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="count-area-content">
                  <div class="count-digit">26</div>
                  <div class="count-title">Internship</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>