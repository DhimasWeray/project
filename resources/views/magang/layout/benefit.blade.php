    <div class="more-info">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="more-info-content">
              <div class="row">
                <div class="col-md-6">
                  <div class="left-image">
                    <img src="{{asset('assets/images/more-info.jpg')}}" alt="">
                  </div>
                </div>
                <div class="col-md-6 align-self-center">
                  <div class="right-content">
                    <span>BENEFIT</span>
                    <h2>Get the benefits by joining <em>our company</em></h2>
                    <p>
By joining us, you will gain work experience for the future. You will also get pocket money every month as long as you join this program.<br><br>You will also get a certificate that will support your career</p>
                   
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>