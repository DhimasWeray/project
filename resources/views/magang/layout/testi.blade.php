    <div class="testimonials">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <h2>What they say <em>about us</em></h2>
              <span>testimonials from our greatest clients</span>
            </div>
          </div>
          <div class="col-md-12">
            <div class="owl-testimonials owl-carousel">

            @foreach ($testi as $key => $testi)
              <div class="testimonial-item">
                <div class="inner-content">
                  
                  <h4>{{$testi->nama}}</h4>
                  <span>{{$testi->position}}</span>
                  <p>{{$testi->testi}}</p>
                </div>
                <!-- <img src="http://placehold.it/60x60" alt=""> -->
              </div>
             @endforeach 

            </div>
          </div>
        </div>
      </div>
    </div>