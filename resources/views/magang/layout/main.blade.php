    <div class="main-banner header-text" id="top">
        <div class="Modern-Slider">
          <!-- Item -->
          <div class="item item-1">
            <div class="img-fill">
                <div class="text-content">
                  <h6>we are ready to help you</h6>
                  <h4>IT Analyst<br>&amp; Consulting</h4>
                  <p>Software development services are needed to help your business connected with your target audience. Worry not, we will make the process as easy and quick as possible. So, what are you waiting for? </p>
                  <a href="#footer" class="filled-button">contact us</a>
                </div>
            </div>
          </div>
          <!-- // Item -->
          <!-- Item -->
          <div class="item item-2">
            <div class="img-fill">
                <div class="text-content">
                  <h6>We are hiring an internship program</h6>
                  <h4>Register<br> Now</h4>
                  <p>we open internship programs for students from various universities and majors. Welcome future careers with valuable work experience</p>
                  <a href="/form" class="filled-button">register</a>
                </div>
            </div>
          <!-- // Item -->

        </div>
    </div>