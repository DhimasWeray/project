<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="TemplateMo">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <title>Finance Business - Services</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/templatemo-finance-business.css') }}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.css')}}">
<!--

Finance Business TemplateMo

https://templatemo.com/tm-545-finance-business

-->
  </head>

  <body>

@include('magang.layout.navbar')

    <!-- Page Content -->
    <div class="page-heading header-text">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>IT SOLUTION</h1>
            <span>INTERNSHIP PROGRAM</span>
          </div>
        </div>
      </div>
    </div>

    <div class="single-services">
      <div class="container">
        <div class="row" id="tabs">
          <div class="col-md-4">
            <ul>
              <li><a >{{$magang->posisi}} <i class="fa fa-angle-right"></i></a></li>
            </ul>
            <div class="container d-flex flex-row-reverse mt-5" >
              <form action="/daftar/program " method="POST">
                @csrf
                  <input name="magang_id" id="magang_id" type="hidden" value="{{$magang->id}}">
                  <input type="hidden" value="{{auth()->user()->id}}" name="user_id" id="user_id">
                  <input name="magang_name" id="magang_name" type="hidden" value="{{$magang->posisi}}">
                  <input type="hidden" value="{{auth()->user()->name}}" name="user_name" id="user_name">
                  <button type="submit" class="filled-button" onclick="myFunction()">Register</button>
              </form>
              </div>
          </div>
          <div class="col-md-8">
            <section class='tabs-content'>
              <article id='tabs'>
                <img src="{{asset('assets/images/'.$magang->image)}}" width="800" height="500" />
                <h4>{{$magang->posisi}}</h4>
                <h5>{{$magang->periode}} Bulan</h5>
                <p>{{$magang->rincian_kegiatan}}</p>    
            </article>
            </section>
          </div>
        </div>
      </div>
    </div>
    



    <div class="partners">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="owl-partners owl-carousel">

            </div>
          </div>
        </div>
      </div>
    </div>



    
</div>
    <div class="sub-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <p>Copyright &copy; 2021 PT.IT Solution
          </p>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>

    <!-- Additional Scripts -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script src="{{asset('assets/js/owl.js')}}"></script>
    <script src="{{asset('assets/js/slick.js')}}"></script>
    <script src="{{asset('assets/js/accordions.js')}}"></script>


    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>

  </body>
</html>
