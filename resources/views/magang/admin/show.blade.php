
@extends('magang.admin.masterAdmin')

@section('content')
<!DOCTYPE html>
<html>
  <head>
    <title>Contact form</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <style>
      html, body {
      min-height: 100%;
      padding: 0;
      margin: 0;
      font-family: Roboto, Arial, sans-serif;
      font-size: 14px;
      color: #666;
      }
      h1 {
      font-weight: 400;
      color: #000000;
      }
      p {
      margin: 0 0 5px;
      }
      .main-block {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      min-height: 100vh;
      background: #ffffff;
      }
      form {
      padding: 25px;
      margin: 25px;
      box-shadow: 0 2px 5px #f5f5f5; 
      background: #f5f5f5; 
      }
      .fa{
      margin: 25px 10px 0;
      font-size: 20px;
      color: rgb(32, 108, 207);
      }
      button {
      width: 100%;
      padding: 10px;
      border: none; 
      font-size: 16px;
      font-weight: 400;
      background-color: #a4c639;
      color: rgb(255, 255, 255);
      font-size: 13px;
      text-transform: uppercase;
      font-weight: 700;
      display: inline-block;
      transition: all 0.3s;
      transition-property: all;
      transition-duration: 0.3s;
      transition-timing-function: ease;
      transition-delay: 0s;
      }
      button:hover {
      background: #ffffff;
      }    
      @media (min-width: 568px) {
      .main-block {
      flex-direction: row;
      }
        }
        td{
          font-size: 20px
        }
        .no{
          font-size: 15px;
        }

        .delete{
          background: transparent;
          box-shadow: none;
        }

        .judul{
          background-color: #a4c639;
        }

    </style>
  </head>
  <body>
    <div class="row mt-5">
        <div class="main-block">
        <div class="container mt-5">
            <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success" >{{session('success')}}</div>
            @endif
            </div>
                <div class="panel-body table-responsive">
                  <div class="display-4 font-weight-bold ">
                    <center>
                    <h1>INTERNSHIP PROGRAM</h1>
                    </center>
                  </div>
                    <br>
                    <table class="table">
                        <thead>
                          
                            <tr>
                                <td class="no">#</td>
                                <td>POSISI</td>
                                <td>PERIODE</td>
                                <td>RINCIAN KEGIATAN</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($magang as $key => $magang)
                                <tr>
                                    <td class="no">{{$key +1}}</td>
                                    <td>{{$magang->posisi}}</td>
                                    <td>{{$magang->periode}}  Bulan</td>
                                    <td>{{$magang->rincian_kegiatan}}</td>
                                    <td>
                                    <ul class="action-list">
                                        <a href="/admin/edit/{{$magang->id}}" data-tip="edit"><i class="fa fa-edit"></i></a>
                                        <form action="/admin/delete/{{$magang->id}}" method="POST" class="fa delete">
                                          @csrf
                                          @method('DELETE')
                                        <button type="submit" class="delete"><i class="fa fa-trash"></i> </button>
                                        </form>
                                    </ul>
                                </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <a href="/admin" class="filled-button mb-5">Add Program</a>
                </div>
            </div>
        </div>
    </div>
</div>
  </div>
  </div>
  </body>
</html>
@endsection