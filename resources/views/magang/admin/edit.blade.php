@extends('magang.admin.masterAdmin')

@section('content')
<!DOCTYPE html>
<html>
  <head>
    <title>Contact form</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <style>
      html, body {
      min-height: 100%;
      padding: 0;
      margin: 0;
      font-family: Roboto, Arial, sans-serif;
      font-size: 14px;
      color: #666;
      }
      h1 {
      margin: 0 0 20px;
      font-weight: 400;
      color: #000000;
      }
      p {
      margin: 0 0 5px;
      }
      .main-block {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      min-height: 100vh;
      background: #ffffff;
      }
      form {
      padding: 25px;
      margin: 25px;
      box-shadow: 0 2px 5px #f5f5f5; 
      background: #f5f5f5; 
      }
      .fas {
      margin: 25px 10px 0;
      font-size: 72px;
      color: rgb(0, 0, 0);
      }
      .fa-envelope {
      transform: rotate(-20deg);
      }
      .fa-at , .fa-mail-bulk{
      transform: rotate(10deg);
      }
      input, textarea {
      width: calc(100% - 18px);
      padding: 8px;
      margin-bottom: 20px;
      border: 1px solid #1c87c9;
      outline: none;
      }
      input::placeholder {
      color: #666;
      }
      button {
      width: 100%;
      padding: 10px;
      border: none; 
      font-size: 16px;
      font-weight: 400;
      background-color: #a4c639;
      color: rgb(255, 255, 255);
      font-size: 13px;
      text-transform: uppercase;
      font-weight: 700;
      display: inline-block;
      transition: all 0.3s;
      transition-property: all;
      transition-duration: 0.3s;
      transition-timing-function: ease;
      transition-delay: 0s;
      }
      button:hover {
      background: #ffffff;
      }    
      @media (min-width: 568px) {
      .main-block {
      flex-direction: row;
      }
      .left-part, form {
      width: 50%;
      }
      .fa-envelope {
      margin-top: 0;
      margin-left: 20%;
      }
      .fa-at {
      margin-top: -10%;
      margin-left: 65%;
      }
      .fa-mail-bulk {
      margin-top: 2%;
      margin-left: 28%;
      }
      }
    </style>
  </head>
  <body>
    <div class="main-block">
      <div class="left-part">
        <i class="fas fa-envelope"></i>
        <i class="fas fa-at"></i>
        <i class="fas fa-mail-bulk"></i>
      </div>

      <form action="/admin/update/{{$magang->id}}" method="POST" enctype='multipart/form-data'>        
        <ul class="list-inline">
          <li><h1>INTERNSHIP PROGRAM</h1></li>
        </ul>

        @csrf
        @method('PUT')
        <div class="info">
          <p>POSISI</p>
          <input type="text" name="posisi" id="posisi" placeholder="Backend Programmer" value=" {{old('posisi', $magang->posisi)}}">
          @error('posisi')
          <div class="alert alert-danger" >{{$message}}</div>
          @enderror
          <p>PERIODE</p>
          <input type="text" name="periode" id="periode" placeholder="Bulan" value=" {{old('periode',$magang->periode)}}">
          @error('periode')
          <div class="alert alert-danger" >{{$message}}</div>
          @enderror
        </div>
        <p>RINCIAN KEGIATAN</p>
        <div>
            <input type="text" name="rincian_kegiatan" id="rincian_kegiatan" placeholder="Rincian Kegiatan" value=" {{old('rincian_kegiatan',$magang->rincian_kegiatan)}}">
          @error('rincian_kegiatan')
          <div class="alert alert-danger " >{{$message}}</div>             
          @enderror
        </div>
          <div>
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">FOTO <span class="required">*</span></label>
          <input type="file" name="file" id="file" >
          @error('file')
              <div class="alert alert-danger " >{{$message}}</div>  
          @enderror
          </div>     
        <button type="submit">Submit</button>
      </form>
    </div>
  </body>
  
</html>
@endsection