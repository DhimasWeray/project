<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\program;
use App\Models\magang;

class PendaftarController extends Controller
{
    public function store(Request $request)
    {
        $pendaftar = new program;

        $pendaftar = program::create([
            "user_id" =>$request["user_id"],
            "magang_id" =>$request["magang_id"],
            "magang_name"=>$request["magang_name"],
            "user_name"=>$request["user_name"]
            ]); 
            $id=$request["magang_id"];
            // dd($pendaftar);
            $magang = magang::find($id);
            // dd($magang);
            return redirect('/program')->with('daftar','Berhasil Mendaftar Program!');
    }

        public function index(Request $request){

        return view('magang.program.show', compact('pendaftar'));
    }
}