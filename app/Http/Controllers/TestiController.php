<?php

namespace App\Http\Controllers;
use App\Models\testi;
use DB;

use Illuminate\Http\Request;

class TestiController extends Controller
{
    public function create(){
        return view('magang.testi.testi');
    }
    public function store(Request $request){
        
        $validate =$request->validate([
            "nama" =>['required'],
            "position" =>['required'],
            "testi" =>['required']
        ]);  
        $testi = new testi;
        $testi = testi::create([
            
            "nama" =>$request["nama"],
            "position" =>$request["position"],
            "testi" =>$request["testi"]
            ]); 
    
        return redirect('/testi/show')->with('success','Berhasil Menambah Data!');
    }
    public function index(){
        $testi = testi::all();
        return view('magang.master',compact('testi'));
    }
}
