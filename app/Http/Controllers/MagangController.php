<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\biodata;
use App\Models\magang;
use DB;


class MagangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function create(){
        return view('magang.registerProgram.masterRegisMagang');
    }

    public function store(Request $request){
        
        $validate =$request->validate([
            "nama" =>['required'],
            "email" =>['required'],
            "alamat" =>['required'],
            "univ" =>['required'],
            "nim" =>['required'],
            "nik" =>['required'],
            'ijazah' =>"required|mimes:pdf|max:10000",
            'cv' =>"required|mimes:pdf|max:10000" 
        ]);  
            
  
      if($request->file('ijazah')) {
         $file = $request->file('ijazah');
         $ijazahname = time().'_'.$file->getClientOriginalName();

         // File upload location
         $location = 'assets/doc';

         // Upload file
         $file->move($location,$ijazahname);
        
      }
      if($request->file('cv')) {
         $file = $request->file('cv');
         $cvname = time().'_'.$file->getClientOriginalName();

         // File upload location
         $location = 'assets/doc';

         // Upload file
         $file->move($location,$cvname);
        
      }
        $biodata = new biodata;
        $biodata = biodata::create([
            
            "nama" =>$request["nama"],
            "email" =>$request["email"],
            "univ" =>$request["univ"],
            "alamat" =>$request["alamat"],
            "nim" =>$request["nim"],
            "nik" =>$request["nik"],
            "ijazah" =>$request["ijazah"],
            "cv" =>$request["cv"],
            "user_id"=>$request["user_id"]
            ]); 
              
            $biodata->ijazah = time().'_'.$ijazahname;//
            $biodata->cv = time().'_'.$cvname;//
             
            $biodata->save();
    
        return redirect('/')->with('success','Berhasil Menambah Data!');
    }
    public function index(){
        $biodata = biodata::all();
        // dd($biodata);
        return view('magang.registerProgram.showApplicant',compact('biodata'));
    }
    
    public function edit($id)
    {
        $biodata=biodata::find($id);
        // dd($biodata);
        return view('magang.registerProgram.edit', compact('biodata'));
    }

    public function update($id,Request $request)
    {
        $validate =$request->validate([
            "nama" =>['required'],
            "email" =>['required'],
            "alamat" =>['required'],
            "univ" =>['required'],
            "nim" =>['required'],
            "nik" =>['required'],
            'ijazah' =>"required|mimes:pdf|max:10000",
            'cv' =>"required|mimes:pdf|max:10000" 
        ]);  

      if($request->file('ijazah')) {
         $file = $request->file('ijazah');
         $ijazahname = time().'_'.$file->getClientOriginalName();

         // File upload location
         $location = 'assets/doc';

         // Upload file
         $file->move($location,$ijazahname);
        
      }
      if($request->file('cv')) {
         $file = $request->file('cv');
         $cvname = time().'_'.$file->getClientOriginalName();

         // File upload location
         $location = 'assets/doc';

         // Upload file
         $file->move($location,$cvname);
        
      }
        
        $biodata = biodata::where('id', $id)->update([
            
            "nama" =>$request["nama"],
            "email" =>$request["email"],
            "univ" =>$request["univ"],
            "alamat" =>$request["alamat"],
            "nim" =>$request["nim"],
            "nik" =>$request["nik"],
            "ijazah"=>$ijazahname,
            "cv"=>$cvname
            // "user_id" => Auth::users()->id
            ]); 
              
        return redirect('/formDaftar/show')->with('success','Berhasil Mengubah Data!');
    }

    public function destroy($id)
    {
        biodata::destroy($id);

        return redirect('/formDaftar/show')->with('success','Berhasil Menghapus Data!');
    }

    public function editUser($id)
    {
        $biodata=biodata::where('user_id',$id)->first();
        // dd($biodata);
        return view('magang.registerProgram.edit', compact('biodata'));
    }


}