<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\magang;

class ProgramController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }
    public function create(){
        return view('magang.admin.form');
    }
    public function store(Request $request){
        $validate =$request->validate([
            "posisi" =>['required',],
            "periode" =>['required'],
            "rincian_kegiatan"=>['required'],
            'file' => 'required|mimes:png,jpg,jpeg|max:2048'
        ]);

      if($request->file('file')) {
         $file = $request->file('file');
         $filename = time().'_'.$file->getClientOriginalName();

         // File upload location
         $location = 'assets/images';

         // Upload file
         $file->move($location,$filename);


      }
        $magang = new magang;

        $magang = magang::create([
            "posisi" =>$request["posisi"],
            "periode" =>$request["periode"],
            "rincian_kegiatan"=>$request["rincian_kegiatan"]
            ]); 
            $magang->image = time().'_'.$request->file->getClientOriginalName();
            $magang->save();

        

        return redirect('/admin/show')->with('success','Berhasil Menambah Data!');
    }

    public function index(){
        $magang = magang::all();
        
        return view('magang.admin.show', compact('magang'));
    }

    public function post(){
        $magang = magang::all();

        return view('magang.program.daftarProgram', compact('magang'));
    } 

    public function rincian($id){
        $magang = magang::find($id);
        // dd($magang);
        return view('magang.program.rincian',compact('magang'));
    }

    public function edit($id)
    {
        $magang=magang::find($id);
        // dd($magang);

        return view('magang.admin.edit', compact('magang'));
    }

    public function update($id,Request $request)
    {
        $validate =$request->validate([
            "posisi" =>['required',],
            "periode" =>['required'],
            "rincian_kegiatan"=>['required'],
            'file' => 'required|mimes:png,jpg,jpeg|max:2048'
        ]);

      if($request->file('file')) {
         $file = $request->file('file');
         $filename = time().'_'.$file->getClientOriginalName();

         // File upload location
         $location = 'assets/images';

         // Upload file
         $file->move($location,$filename);


      }

        $magang = magang::where('id', $id)->update([
            "posisi" =>$request["posisi"],
            "periode" =>$request["periode"],
            "rincian_kegiatan"=>$request["rincian_kegiatan"],
             "image" => $filename
            ]); 
           
            

        

        return redirect('/admin/show')->with('success','Berhasil Mengubah Data!');
    }

    public function destroy($id)
    {
        magang::destroy($id);

        return redirect('/admin/show')->with('success','Berhasil Menghapus Data!');
    }
}